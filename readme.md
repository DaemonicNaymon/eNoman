# Nostore Webshop

**NOTE**: for mollie to work, this webshop needs to be located inside `xampp/htdocs/projects/` and `composer install` must've been run in the terminal.
**Nostore** self, is the **project root** and *web* is the *webroot*.
 

## Setting up the database
In the zip you will find the .sql file, dont lose it!
 1. **go** to your [local phpmyadmin](http://localhost/phpmyadmin)
 2. **create** a new database named `nostore`
 3. **import** the sql file and the tables should be there with their values!
 4. **fill in** the **database settings** in `Database.php` class
## Setting up the webshop
>if you already set up the database and its working, you can just throw away that sql file
 1. **Extract the zip** 
 2. **move** the `Nostore` folder to `xampp/htdocs/projects/` and that should be it!

## Setting up composer
after the webshop has been placed correctly in the `xampp/htdocs/projects/` folder
1. **open** a terminal
2. **move** to the `Nostore` directory
3. **run** the following command: `composer install` 
4. **go** to the **project root** of `Nostore` to check if there is a `vendor` folder
5. thats it! 
## Setting up mollie and ngrok
for mollie to work, it needs a tunnel from localhost, that's where ngrok comes in. 
on **Line 91** in `PayController.php` there is the following code: 
`$webhookUrl = (router()->domainName == 'localhost' || router()->domainName == 'enoman.com') ? $this->ngrok.'/projects/Nostore/web/pay/webhook/' . $orderId : router()->name('pay.webhook', ['orderId' => $orderId]);`

`$this->ngrok` is a variable on top of `PayController.php` for the forward url.
Example: `https://2f3dcad7.ngrok.io`. You should also find the  API key for **mollie** on top. 

before testing mollie payments
### mollie
1. **go** to [mollie.com](https://www.mollie.com/en/) then to dashboard for the **TEST API key**
2. **turn on** test mode while you are there.
3.  **change** the **API key** on **Line 14** in `PayController.php`
### ngrok
1. **get** ngrok [here](https://ngrok.com)
2. **go** to terminal or ngrok.exe to run `ngrok http 80`
3. **look** for a forward url which looks like the example above and **copy** it.
6.   **change/paste** the **url** on **Line 17** in `PayController.php` 
