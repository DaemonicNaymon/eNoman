<!DOCTYPE html>
<html lang="en">
<head>
    <?php include PATH_APP_VIEWS . 'partials/head.html.php'; ?>
    <title>eNoman</title>
</head>
<body>
<?php include PATH_APP_VIEWS . 'partials/branding.html.php'; ?>
<!-- End site branding area -->

<?php include PATH_APP_VIEWS . 'partials/menu.html.php'; ?>
<!-- End mainmenu area -->

<div class="slider-area">
    <div class="zigzag-bottom"></div>
    <div id="slide-list" class="carousel carousel-fade slide" data-ride="carousel">

        <div class="slide-bulletz">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="carousel-indicators slide-indicators">
                            <li data-target="#slide-list" data-slide-to="0" class="active"></li>
                            <li data-target="#slide-list" data-slide-to="1"></li>
                            <li data-target="#slide-list" data-slide-to="2"></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="single-slide">
                    <div class="slide-bg slide-one"></div>
                    <div class="slide-text-wrapper">
                        <div class="slide-text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="slide-content">
                                            <h2>I am awesome</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur,
                                                dolorem, excepturi. Dolore aliquam quibusdam ut quae iure vero
                                                exercitationem ratione!</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi ab
                                                molestiae minus reiciendis! Pariatur ab rerum, sapiente ex nostrum
                                                laudantium.</p>
                                            <a href="" class="readmore">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="single-slide">
                    <div class="slide-bg slide-two"></div>
                    <div class="slide-text-wrapper">
                        <div class="slide-text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="slide-content">
                                            <h2>I am great</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe
                                                aspernatur, dolorum harum molestias tempora deserunt voluptas possimus
                                                quos eveniet, vitae voluptatem accusantium atque deleniti inventore.
                                                Enim quam placeat expedita! Quibusdam!</p>
                                            <a href="" class="readmore">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="single-slide">
                    <div class="slide-bg slide-three"></div>
                    <div class="slide-text-wrapper">
                        <div class="slide-text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="slide-content">
                                            <h2>I am superb</h2>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores,
                                                eius?</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti
                                                voluptates necessitatibus dicta recusandae quae amet nobis sapiente
                                                explicabo voluptatibus rerum nihil quas saepe, tempore error odio quam
                                                obcaecati suscipit sequi.</p>
                                            <a href="" class="readmore">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div> <!-- End slider area -->

<div class="promo-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-promo">
                    <i class="fa fa-refresh"></i>
                    <p>0 Days return</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-promo">
                    <i class="fa fa-truck"></i>
                    <p>Expensive shipping</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-promo">
                    <i class="fa fa-lock"></i>
                    <p>Unsecure payments</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-promo">
                    <i class="fa fa-gift"></i>
                    <p>Very old products</p>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End promo area -->

<div class="maincontent-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="latest-product">
                    <h2 class="section-title">Latest Products</h2>
                    <div class="product-carousel">

                        <?php foreach ($products as $product) { ?>
                        <div class="single-product">
                            <div class="product-f-image">
                                <img src="<?php echo 'img/' . $product->image; ?>" alt="<?php $product->slug; ?>">
                                <div class="product-hover">
                                    <button id="clickMe" class="add-to-cart-link"
                                            data-url="<?php echo router()->name('cart.add', ['id' => $product->id]); ?>">
                                        <i class="fa fa-shopping-cart"></i> Add to cart
                                    </button>
                                    <a href="<?php echo router()->name('product', ['slug' => $product->slug]); ?>"
                                       class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                </div>
                            </div>

                            <h2>
                                <a href="<?php echo router()->name('product', ['slug' => $product->slug]); ?>"><?php echo $product->title; ?></a>
                            </h2>
                            <div class="product-carousel-price">
                                <ins>&euro; <?php echo $product->price; ?></ins>
                            </div>
                        </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End main content area -->

<div class="brands-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="brand-wrapper">
                    <h2 class="section-title">Brands</h2>
                    <div class="brand-list">
                        <img src="<?php echo asset('img/services_logo__1.jpg'); ?>" alt="">
                        <img src="<?php echo asset('img/services_logo__2.jpg'); ?>" alt="">
                        <img src="<?php echo asset('img/services_logo__3.jpg'); ?>" alt="">
                        <img src="<?php echo asset('img/services_logo__4.jpg'); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End brands area -->

<!--JavaScript library + footer -->
<?php include PATH_APP_VIEWS . 'partials/footer.html.php'; ?>
<?php include PATH_APP_VIEWS . 'partials/jslib.html.php'; ?>
<a class="back-top-button"></a>
</body>
</html>