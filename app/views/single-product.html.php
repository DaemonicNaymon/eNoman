<!DOCTYPE html>
<html lang="en">
<head>
    <?php include PATH_APP_VIEWS . 'partials/head.html.php' ?>
    <meta name="title" content="<?php echo $product->seo_title; ?>">
    <meta name="description" content="<?php echo $product->seo_description; ?>">
    <title><?php echo $product->title; ?></title>
</head>
<body>
<?php include PATH_APP_VIEWS . 'partials/branding.html.php' ?>
<!-- End site branding area -->
<?php include PATH_APP_VIEWS . 'partials/menu.html.php' ?>
<!-- End mainmenu area -->

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2><?php echo $product->title; ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-content-right">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="product-images">
                                <div class="product-main-img">
                                    <img src="<?php echo asset('img/' . $product->image); ?>"
                                         alt="<?php $product->slug; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="product-inner">
                                <div class="product-inner-price col-sm-12">
                                    <ins class="bigger">&euro; <?php echo $product->price ?></ins>
                                    <button class="add-to-cart-link"
                                            data-url="<?php echo router()->name('cart.add', ['id' => $product->id]); ?>"
                                            type="submit" style="float:right;">
                                        Add to cart
                                    </button>
                                </div>
                                <div role="tabpanel">
                                    <ul class="product-tab" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                                  role="tab" data-toggle="tab">Description</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                                            <div class="wrapper">
                                                <h2>Product Description</h2>
                                                <?php echo $product->description; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--JavaScript library + footer -->
<?php include PATH_APP_VIEWS . 'partials/footer.html.php'; ?>
<?php include PATH_APP_VIEWS . 'partials/jslib.html.php'; ?>
<a class="back-top-button"></a>
</body>
</html>