<!-- Latest jQuery from server -->
<script src="https://code.jquery.com/jquery.min.js"></script>

<!-- Bootstrap JS from CDN -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="<?php echo asset('js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo asset('js/jquery.sticky.js'); ?>"></script>

<!-- jQuery easing -->
<script src="<?php echo asset('js/jquery.easing.1.3.min.js'); ?>"></script>

<!-- Main Script -->
<script src="<?php echo asset('js/main.js'); ?>"></script>
