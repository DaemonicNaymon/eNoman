<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-about-us">
                    <h2>Noman's <span>Electronics</span></h2>
                    <p>Hi im Noman, this is my store, feel free to waste your money on useless products I clearly don't
                        have for sale.</p>
                    <div class="footer-social">
                        <a href="#" target="_blank"><i class="fa fa-github-alt"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-gitlab"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; eNoman 2018. All Rights Reserved. Coded with <i class="fa fa-heart"></i></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                    <i class="fab fa-ethereum"></i>
                </div>
            </div>
        </div>
    </div>
</div>