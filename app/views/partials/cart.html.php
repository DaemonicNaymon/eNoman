<div class="shopping-item">
    <a id="cartModalOpener"
       class="get-cart-modal"
       data-toggle="modal"
       data-backdrop="false"
       data-keyboard="false"
       data-target="#cartModal"
       data-url="<?php echo router()->name('cart.get.modal'); ?>">Cart -
        <span class="cart-amount">&euro;<?php echo $_SESSION['cart']['total']; ?></span>
        <i class="fa fa-shopping-cart"></i>
        <span class="product-count">
            <?php echo ($_SESSION['cart']['count'] = (isset($_SESSION['cart']['count']) ? $_SESSION['cart']['count'] : 0));?>
        </span>
    </a>
</div>
