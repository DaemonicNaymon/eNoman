<!DOCTYPE html>
<html lang="en">
<head>
    <?php include PATH_APP_VIEWS . 'partials/head.html.php'; ?>
</head>
<body>
<?php include PATH_APP_VIEWS . 'partials/branding.html.php'; ?>
<!-- End site branding area -->
<?php include PATH_APP_VIEWS . 'partials/menu.html.php' ?>
<!-- End mainmenu area -->

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>Checkout</h2>
                    <?php if (router()->errors()) { ?>
                        <div class="alert alert-danger">
                            Woops, not everything is filled in correctly.
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="single-product-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-content-right">
                    <div class="checkout">
                        <div class="checkout-info">Returning customer?
                            <a class="showlogin" data-toggle="collapse"
                               href="#login-form-wrap" aria-expanded="false"
                               aria-controls="login-form-wrap">Click here to login</a>
                        </div>

                        <form id="login-form-wrap" class="login collapse" method="post">

                            <p>If you have shopped with us before, please enter your details in the boxes below. If you
                                are a new customer please proceed to the Billing &amp; Shipping section.</p>

                            <p class="form-row form-row-first">
                                <label for="username">email<span class="required">*</span>
                                </label>
                                <input type="text" placeholder="example@example.com" id="username" name="username"
                                       class="input-text">
                            </p>
                            <p class="form-row form-row-last">
                                <label for="password">Password <span class="required">*</span>
                                </label>
                                <input type="password" id="password" name="password" class="input-text">
                            </p>


                            <p class="form-row">
                                <input type="submit" value="Login" name="login" class="button">
                                <!--                                    <label class="inline" for="rememberme"><input type="checkbox" value="forever" id="rememberme" name="rememberme"> Remember me </label>-->
                            </p>
                        </form>

                        <form class="checkout" action="<?php echo router()->name('pay.create'); ?>" method="post"
                              name="checkout">

                            <div id="customer_details" class="col2-set">
                                <h3 class="billing-title">Billing Details</h3>
                                <div class="billing-fields">
                                    <fieldset>
                                        <p id="billing_country_field"
                                           class="form-row form-row-wide address-field update_totals_on_change validate-required woocommerce-validated">
                                            <label class="" for="billing_country">Country <abbr title="required"
                                                                                                class="required">*</abbr></label>
                                            <select class="country_to_state country_select" id="billing_country"
                                                    name="country">

                                                <?php foreach ([
                                                    'AX' => 'Åland Islands',
                                                    'AF' => 'Afghanistan',
                                                    'AL' => 'Albania',
                                                    'DZ' => 'Algeria',
                                                    'AD' => 'Andorra',
                                                    'AO' => 'Angola',
                                                    'AI' => 'Anguilla',
                                                    'AQ' => 'Antarctica',
                                                    'AG' => 'Antigua and Barbuda',
                                                    'AR' => 'Argentina',
                                                    'AM' => 'Armenia',
                                                    'AW' => 'Aruba',
                                                    'AU' => 'Australia',
                                                    'AT' => 'Austria',
                                                    'AZ' => 'Azerbaijan',
                                                    'BS' => 'Bahamas',
                                                    'BH' => 'Bahrain',
                                                    'BD' => 'Bangladesh',
                                                    'BB' => 'Barbados',
                                                    'BY' => 'Belarus',
                                                    'PW' => 'Palau',
                                                    'BE' => 'Belgium',
                                                    'BZ' => 'Belize',
                                                    'BJ' => 'Benin',
                                                    'BM' => 'Bermuda',
                                                    'BT' => 'Bhutan',
                                                    'BO' => 'Bolivia',
                                                    'BQ' => 'Bonaire, Saint Eustatius and Saba',
                                                    'BA' => 'Bosnia and Herzegovina',
                                                    'BW' => 'Botswana',
                                                    'BV' => 'Bouvet Island',
                                                    'BR' => 'Brazil',
                                                    'IO' => 'British Indian Ocean Territory',
                                                    'VG' => 'British Virgin Islands',
                                                    'BN' => 'Brunei',
                                                    'BG' => 'Bulgaria',
                                                    'BF' => 'Burking Faso',
                                                    'BI' => 'Burundi',
                                                    'KH' => 'Cambodia',
                                                    'CM' => 'Cameroon',
                                                    'CA' => 'Canada',
                                                    'CV' => 'Cape Verde',
                                                    'KY' => 'Cayman Islands',
                                                    'CF' => 'Central African Republic',
                                                    'TD' => 'Chad',
                                                    'CL' => 'Chile',
                                                    'CN' => 'China',
                                                    'CX' => 'Christmas Island',
                                                    'CC' => 'Cocos (Keeling) Islands',
                                                    'CO' => 'Colombia',
                                                    'KM' => 'Comoros',
                                                    'CG' => 'Congo (Brazzaville)',
                                                    'CD' => 'Congo (Kinshasa)',
                                                    'CK' => 'Cook Islands',
                                                    'CR' => 'Costa Rica',
                                                    'HR' => 'Croatia',
                                                    'CU' => 'Cuba',
                                                    'CW' => 'CuraÇao',
                                                    'CY' => 'Cyprus',
                                                    'CZ' => 'Czech Republic',
                                                    'DK' => 'Denmark',
                                                    'DJ' => 'Djibouti',
                                                    'DM' => 'Dominica',
                                                    'DO' => 'Dominican Republic',
                                                    'EC' => 'Ecuador',
                                                    'EG' => 'Egypt',
                                                    'SV' => 'El Salvador',
                                                    'GQ' => 'Equatorial Guinea',
                                                    'ER' => 'Eritrea',
                                                    'EE' => 'Estonia',
                                                    'ET' => 'Ethiopia',
                                                    'FK' => 'Falkland Islands',
                                                    'FO' => 'Faroe Islands',
                                                    'FJ' => 'Fiji',
                                                    'FI' => 'Finland',
                                                    'FR' => 'France',
                                                    'GF' => 'French Guiana',
                                                    'PF' => 'French Polynesia',
                                                    'TF' => 'French Southern Territories',
                                                    'GA' => 'Gabon',
                                                    'GM' => 'Gambia',
                                                    'GE' => 'Georgia',
                                                    'DE' => 'Germany',
                                                    'GH' => 'Ghana',
                                                    'GI' => 'Gibraltar',
                                                    'GR' => 'Greece',
                                                    'GL' => 'Greenland',
                                                    'GD' => 'Grenada',
                                                    'GP' => 'Guadeloupe',
                                                    'GT' => 'Guatemala',
                                                    'GG' => 'Guernsey',
                                                    'GN' => 'Guinea',
                                                    'GW' => 'Guinea-Bissau',
                                                    'GY' => 'Guyana',
                                                    'HT' => 'Haiti',
                                                    'HM' => 'Heard Island and McDonald Islands',
                                                    'HN' => 'Honduras',
                                                    'HK' => 'Hong Kong',
                                                    'HU' => 'Hungary',
                                                    'IS' => 'Iceland',
                                                    'IN' => 'India',
                                                    'ID' => 'Indonesia',
                                                    'IR' => 'Iran',
                                                    'IQ' => 'Iraq',
                                                    'IM' => 'Isle of Man',
                                                    'IL' => 'Israel',
                                                    'IT' => 'Italy',
                                                    'CI' => 'Ivory Coast',
                                                    'JM' => 'Jamaica',
                                                    'JP' => 'Japan',
                                                    'JE' => 'Jersey',
                                                    'JO' => 'Jordan',
                                                    'KZ' => 'Kazakhstan',
                                                    'KE' => 'Kenya',
                                                    'KI' => 'Kiribati',
                                                    'KW' => 'Kuwait',
                                                    'KG' => 'Kyrgyzstan',
                                                    'LA' => 'Laos',
                                                    'LV' => 'Latvia',
                                                    'LB' => 'Lebanon',
                                                    'LS' => 'Lesotho',
                                                    'LR' => 'Liberia',
                                                    'LY' => 'Libya',
                                                    'LI' => 'Liechtenstein',
                                                    'LT' => 'Lithuania',
                                                    'LU' => 'Luxembourg',
                                                    'MO' => 'Macao S.A.R., China',
                                                    'MK' => 'Macedonia',
                                                    'MG' => 'Madagascar',
                                                    'MW' => 'Malawi',
                                                    'MY' => 'Malaysia',
                                                    'MV' => 'Maldives',
                                                    'ML' => 'Mali',
                                                    'MT' => 'Malta',
                                                    'MH' => 'Marshall Islands',
                                                    'MQ' => 'Martinique',
                                                    'MR' => 'Mauritania',
                                                    'MU' => 'Mauritius',
                                                    'YT' => 'Mayotte',
                                                    'MX' => 'Mexico',
                                                    'FM' => 'Micronesia',
                                                    'MD' => 'Moldova',
                                                    'MC' => 'Monaco',
                                                    'MN' => 'Mongolia',
                                                    'ME' => 'Montenegro',
                                                    'MS' => 'Montserrat',
                                                    'MA' => 'Morocco',
                                                    'MZ' => 'Mozambique',
                                                    'MM' => 'Myanmar',
                                                    'NA' => 'Namibia',
                                                    'NR' => 'Nauru',
                                                    'NP' => 'Nepal',
                                                    'NL' => 'Netherlands',
                                                    'AN' => 'Netherlands Antilles',
                                                    'NC' => 'New Caledonia',
                                                    'NZ' => 'New Zealand',
                                                    'NI' => 'Nicaragua',
                                                    'NE' => 'Niger',
                                                    'NG' => 'Nigeria',
                                                    'NU' => 'Niue',
                                                    'NF' => 'Norfolk Island',
                                                    'KP' => 'North Korea',
                                                    'NO' => 'Norway',
                                                    'OM' => 'Oman',
                                                    'PK' => 'Pakistan',
                                                    'PS' => 'Palestinian Territory',
                                                    'PA' => 'Panama',
                                                    'PG' => 'Papua New Guinea',
                                                    'PY' => 'Paraguay',
                                                    'PE' => 'Peru',
                                                    'PH' => 'Philippines',
                                                    'PN' => 'Pitcairn',
                                                    'PL' => 'Poland',
                                                    'PT' => 'Portugal',
                                                    'QA' => 'Qatar',
                                                    'IE' => 'Republic of Ireland',
                                                    'RE' => 'Reunion',
                                                    'RO' => 'Romania',
                                                    'RU' => 'Russia',
                                                    'RW' => 'Rwanda',
                                                    'ST' => 'São Tomé and Príncipe',
                                                    'BL' => 'Saint Barthélemy',
                                                    'SH' => 'Saint Helena',
                                                    'KN' => 'Saint Kitts and Nevis',
                                                    'LC' => 'Saint Lucia',
                                                    'SX' => 'Saint Martin (Dutch part)',
                                                    'MF' => 'Saint Martin (French part)',
                                                    'PM' => 'Saint Pierre and Miquelon',
                                                    'VC' => 'Saint Vincent and the Grenadines',
                                                    'SM' => 'Saint Marino',
                                                    'SA' => 'Saudi Arabia',
                                                    'SN' => 'Senegal',
                                                    'RS' => 'Serbia',
                                                    'SC' => 'Seychelles',
                                                    'SL' => 'Sierra Leone',
                                                    'SG' => 'Singapore',
                                                    'SK' => 'Slovakia',
                                                    'SI' => 'Slovenia',
                                                    'SB' => 'Solomon Islands',
                                                    'SO' => 'Somalia',
                                                    'ZA' => 'South Africa',
                                                    'GS' => 'South Georgia/Sandwich Islands',
                                                    'KR' => 'South Korea',
                                                    'SS' => 'South Sudan',
                                                    'ES' => 'Spain',
                                                    'LK' => 'Sri Lanka',
                                                    'SD' => 'Sudan',
                                                    'SR' => 'Suriname',
                                                    'SJ' => 'Svalbard and Jan Mayen',
                                                    'SZ' => 'Swaziland',
                                                    'SE' => 'Sweden',
                                                    'CH' => 'Switzerland',
                                                    'SY' => 'Syria',
                                                    'TW' => 'Taiwan',
                                                    'TJ' => 'Tajikistan',
                                                    'TZ' => 'Tanzania',
                                                    'TH' => 'Thailand',
                                                    'TL' => 'Timor-Leste',
                                                    'TG' => 'Togo',
                                                    'TK' => 'Tokelau',
                                                    'TO' => 'Tonga',
                                                    'TT' => 'Trinidad and Tobago',
                                                    'TN' => 'Tunisia',
                                                    'TR' => 'Turkey',
                                                    'TM' => 'Turkmenistan',
                                                    'TC' => 'Turks and Caicos Islands',
                                                    'TV' => 'Tuvalu',
                                                    'UG' => 'Uganda',
                                                    'UA' => 'Ukraine',
                                                    'AE' => 'United Arab Emirates',
                                                    'GB' => 'United Kingdom (UK)',
                                                    'US' => 'United States (US)',
                                                    'UY' => 'Uruguay',
                                                    'UZ' => 'Uzbekistan',
                                                    'VU' => 'Vanuatu',
                                                    'VA' => 'Vatican',
                                                    'VE' => 'Venezuela',
                                                    'VN' => 'Wallis and Futuna',
                                                    'EH' => 'Western Sahara',
                                                    'WS' => 'Western Samoa',
                                                    'YE' => 'Yemen',
                                                    'ZM' => 'Zambia',
                                                    'ZW' => 'Zimbabwe',
                                                ] as $iso => $country) { ?>
                                                    <option value="<?php echo $iso; ?>" <?php echo (router()->value('country')) ? 'selected="selected"' : ''; ?>><?php echo $country; ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo (router()->errors('country')) ? '<p class="text-danger">' . router()->errors('country')[0] . '</p>' : ''; ?>
                                        </p>

                                        <p id="billing_first_name_field"
                                           class="form-row form-row-first validate-required">
                                            <label class="" for="billing_first_name">First Name <abbr title="required"
                                                                                                      class="required">*</abbr>
                                            </label>
                                            <input type="text" placeholder="first name" id="billing_first_name"
                                                   name="first_name" class="input-text"
                                                   value="<?php echo router()->value('first_name'); ?>">
                                            <?php echo (router()->errors('first_name')) ? '<p class="text-danger">' . router()->errors('first_name')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="billing_suffix_field" class="form-row form-row-wide">
                                            <label class="" for="billing_suffix">Suffix</label>
                                            <input type="text" value="<?php echo router()->value('suffix_name'); ?>"
                                                   placeholder="suffix" id="billing_suffix" name="suffix_name"
                                                   class="input-text">
                                            <?php echo (router()->errors('suffix_name')) ? '<p class="text-danger">' . router()->errors('suffix_name')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="billing_last_name_field"
                                           class="form-row form-row-last validate-required">
                                            <label class="" for="billing_last_name">Last Name <abbr title="required"
                                                                                                    class="required">*</abbr>
                                            </label>
                                            <input type="text" value="<?php echo router()->value('last_name'); ?>"
                                                   placeholder="last name" id="billing_last_name" name="last_name"
                                                   class="input-text">
                                            <?php echo (router()->errors('last_name')) ? '<p class="text-danger">' . router()->errors('last_name')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="billing_email_field"
                                           class="form-row form-row-first validate-required validate-email">
                                            <label class="" for="billing_email">Email Address <abbr title="required"
                                                                                                    class="required">*</abbr>
                                            </label>
                                            <input type="text" value="<?php echo router()->value('email'); ?>"
                                                   placeholder="example@example.com" id="billing_email" name="email"
                                                   class="input-text">
                                            <?php echo (router()->errors('email')) ? '<p class="text-danger">' . router()->errors('email')[0] . '</p>' : ''; ?>
                                        </p>
                                    </fieldset>

                                    <fieldset>
                                        <p id="billing_address_field"
                                           class="form-row form-row-wide address-field validate-required">
                                            <label class="" for="billing_address">Address <abbr title="required"
                                                                                                class="required">*</abbr>
                                            </label>
                                            <input type="text" value="<?php echo router()->value('address'); ?>"
                                                   placeholder="Street address" id="billing_address" name="address"
                                                   class="input-text">
                                            <?php echo (router()->errors('address')) ? '<p class="text-danger">' . router()->errors('address')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="billing_house_number_field"
                                           class="form-row form-row-wide address-field validate-required">
                                            <label class="" for="billing_house_number">House number<abbr
                                                        title="required" class="required">*</abbr>
                                            </label>
                                            <input type="text" placeholder="house number" id="billing_house_number"
                                                   name="house_number" class="input-text"
                                                   value="<?php echo router()->value('house_number'); ?>">
                                            <?php echo (router()->errors('house_number')) ? '<p class="text-danger">' . router()->errors('house_number')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="billing_house_number_suffix_field"
                                           class="form-row form-row-wide address-field validate-required">
                                            <label class="" for="billing_house_number_suffix">House Number suffix
                                            </label>
                                            <input type="text"
                                                   value="<?php echo router()->value('house_number_suffix'); ?>"
                                                   placeholder="add on" id="billing_house_number_suffix"
                                                   name="house_number_suffix" class="input-text">
                                            <?php echo (router()->errors('house_number_suffix')) ? '<p class="text-danger">' . router()->errors('house_number_suffix')[0] . '</p>' : ''; ?>
                                        </p>

                                        <p id="billing_city_field"
                                           class="form-row form-row-wide address-field validate-required"
                                           data-o_class="form-row form-row-wide address-field validate-required">
                                            <label class="" for="billing_city">City <abbr title="required"
                                                                                          class="required">*</abbr>
                                            </label>
                                            <input type="text" value="<?php echo router()->value('city'); ?>"
                                                   placeholder="City" id="billing_city" name="city" class="input-text">
                                            <?php echo (router()->errors('city')) ? '<p class="text-danger">' . router()->errors('city')[0] . '</p>' : ''; ?>
                                        </p>

                                        <p id="billing_postcode_field"
                                           class="form-row form-row-last address-field validate-required validate-postcode"
                                           data-o_class="form-row form-row-last address-field validate-required validate-postcode">
                                            <label class="" for="billing_postcode">Postcode <abbr title="required"
                                                                                                  class="required">*</abbr>
                                            </label>
                                            <input type="text" value="<?php echo router()->value('postcode'); ?>"
                                                   placeholder="Postcode / Zip" id="billing_postcode" name="postcode"
                                                   class="input-text">
                                            <?php echo (router()->errors('postcode')) ? '<p class="text-danger">' . router()->errors('postcode')[0] . '</p>' : ''; ?>
                                        </p>
                                    </fieldset>


                                    <div class="create-account">
                                        <p>Create an account by entering the information below. If you are a returning
                                            customer please login at the top of the page.</p>
                                        <p id="account_password_field" class="form-row validate-required">
                                            <label class="" for="account_password">Account password <abbr
                                                        title="required" class="required">*</abbr>
                                            </label>
                                            <input type="password" value="" placeholder="Password" id="account_password"
                                                   name="password" class="input-text">
                                            <?php echo (router()->errors('password')) ? '<p class="text-danger">' . router()->errors('password')[0] . '</p>' : ''; ?>
                                        </p>
                                        <p id="account_password_field" class="form-row validate-required">
                                            <label class="" for="password_confirm">Confirm password <abbr
                                                        title="required" class="required">*</abbr>
                                            </label>
                                            <input type="password" value="" placeholder="Password" id="password_confirm"
                                                   name="password_confirm" class="input-text">
                                        </p>
                                    </div>

                                </div>

                            </div>

                            <h3 class="billing-title" id="order_review_heading">Your order</h3>

                            <div id="order_review" style="position: relative;">
                                <table class="shop_table">
                                    <thead>
                                    <tr>
                                        <th class="product-name">Product</th>
                                        <th class="product-total">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($_SESSION['cart']['products']) { ?>
                                        <?php foreach ($_SESSION['cart']['products'] as $item) { ?>
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    <?php echo $item['title'] ?> <strong
                                                            class="product-quantity">× <?php echo $item['quantity']; ?></strong>
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount">&euro; <?php echo $item['price'] * $item['quantity']; ?></span>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <tr class="cart_item">
                                            <td class="red-color" colspan="2"><em>no products</em> 😟</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="shipping">
                                        <th>Shipping and Handling</th>
                                        <td>
                                            Free Shipping
                                            <input type="hidden" class="shipping_method" value="free_shipping"
                                                   id="shipping_method_0" data-index="0" name="shipping_method[0]">
                                        </td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Order Total</th>
                                        <td><strong><span
                                                        class="amount">&euro; <?php echo $_SESSION['cart']['total']; ?></span></strong>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div id="payment">
                                    <div class="form-row place-order">
                                        <input type="submit" value="Place order" id="place_order"
                                               name="checkout_place_order" class="button alt">
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--JavaScript library + footer -->
<?php include PATH_APP_VIEWS . 'partials/footer.html.php'; ?>
<?php include PATH_APP_VIEWS . 'partials/jslib.html.php'; ?>
<a class="back-top-button"></a>
</body>
</html>