<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 15/05/2018
 * Time: 20:58
 */

class HomeController extends Controller
{
    public function index()
    {
        $products = db()->query('SELECT * FROM products')
            ->orderBy('id', 'DESC')
            ->limit(5)
            ->select('Product');

        return response()->view('index')
            ->with('products', $products);
    }
}