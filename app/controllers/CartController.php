<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 23/05/2018
 * Time: 19:49
 */

class CartController extends Controller
{
    // add function
    public function add($id)
    {
        Cart::addToCart($id);
        return response()->view('partials/cart');
    }
    // remove function
    public function remove($id)
    {
        Cart::removeFromCart($id);
        return response()->view('partials/cart');
    }
    // add button in modal function, please forgive me for using 5 ajax calls without JSON.
    public function addItemModal($id)
    {
        Cart::addToCart($id);
        return response()->view('partials/cart-modal-content');
    }
    // remove button in modal function
    public function removeItemModal($id)
    {
        Cart::removeFromCart($id);
        return response()->view('partials/cart-modal-content');
    }
    // update the modal function
    public function getModal()
    {
        Cart::get();
        return response()->view('partials/cart-modal-content');
    }
    // update function
    public function get()
    {
        Cart::get();
        return response()->view('partials/cart');
    }
    // reset the cart function
    public function reset()
    {
        Cart::reset();
        return response()->view('partials/cart-modal-content');
    }
}