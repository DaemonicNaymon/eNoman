<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 16/05/2018
 * Time: 13:24
 */

use Mollie\Api\Exceptions\ApiException;

Class PayController extends Controller
{
    // Mollie API key, mollie.com is where you can get it.
    private $mollieApiKey = 'test_b9pWcPp5CawyPy7JehjgqC2Ws9gghn';
    // ngrok forward url, notice: it needs to expose the whole localhost for the webshop to work.
    // for more info, look at line 90
    private $ngrok = 'https://2f3dcad7.ngrok.io';

    public function index()
    {
        return response()->view('pay');
    }

    public function create()
    {
        // validate the user data
        require PATH_APP_PAYMENT . 'PayValidationCreate.php';
        try {
            /*
                There is always a posibility that queries don't run as expected.
                When one fails, another might succeed. That means that you might
                have inconsistent data in your database.
                To counteract this, you can start a transaction. A transaction
                is running a query, but not committing to it until you are ready
                for it. To run them, you just need to commit.
            */
            db()->transaction();

            $parameters = router()->parameters();

            /*
             * database format functions, very important
             */
            function standardizePostcode($postcode)
            {
                return strtoupper(chunk_split($postcode, 4, ' '));
            }


            function standardizeName($string)
            {
                return ucwords(trim($string));
            }

            // create the user and insert it into the database formatted
            $userId = db()->query('INSERT INTO `users` (first_name, suffix_name, last_name, country, city, street, street_number, street_suffix, zipcode, email, password, created_at, updated_at)
				VALUES
				(:first_name, :suffix_name, :last_name, :country, :city, :street, :street_number, :street_suffix, :zipcode, :email, :password, :created_at, :updated_at)', [
                'first_name' => standardizeName($parameters['first_name']),
                'suffix_name' => trim($parameters['suffix_name']),
                'last_name' => standardizeName($parameters['last_name']),
                'country' => $parameters['country'],
                'city' => standardizeName($parameters['city']),
                'street' => standardizeName($parameters['address']),
                'street_number' => $parameters['house_number'],
                'street_suffix' => trim($parameters['street_suffix']),
                'zipcode' => standardizePostcode($parameters['postcode']),
                'email' => strtolower(trim($parameters['email'])),
                // encrypt the password before putting it in the database
                // future reference, password_verify checks if a password matches a hash
                'password' => password_hash($parameters['password'], PASSWORD_BCRYPT),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ])->insert();

            // create an order in the database, update the mollie id after
            $orderId = db()->query('INSERT INTO `orders`
				(amount, payment_status, user_id, created_at, updated_at)
				VALUES
				(:amount, :payment_status, :user_id, :created_at, :updated_at)', [
                'amount' => $_SESSION['cart']['total'],
                'payment_status' => 'open',
                'user_id' => $userId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ])->insert();

            // On localhost mollie can't reach us. So https://ngrok.com/ will tunnel for us on localhost.
            // But on the server it will not, only on localhost or virtualhost (enoman.com)
            // why ngrok has such a long url is because Ive called the `ngrok http 80` on localhost instead of the project itself, so my whole localhost is exposed
            $webhookUrl = (router()->domainName == 'localhost' || router()->domainName == 'enoman.com') ? $this->ngrok.'/projects/Nostore/web/pay/webhook/' . $orderId : router()->name('pay.webhook', ['orderId' => $orderId]);

            // instantiate mollie API for payment!
            $mollie = new \Mollie\Api\MollieApiClient();
            $mollie->setApiKey($this->mollieApiKey); // test api key

            // create a mollie api v2 payment: https://github.com/mollie/mollie-api-php
            $payment = $mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    /*
                     * Line 88: keep sprintf function because the cart total is an int,
                     * not a double, which mollie needs it to be so it can handle it
                     * otherwise, you will get an "unprocessable entity" error, telling the amount has an invalid value.
                     */
                    "value" => (string)sprintf("%.2f", $_SESSION['cart']['total'])
                ],
                "description" => "Bedankt voor uw aankoop bij Nostore!",
                "redirectUrl" => router()->name('pay.done', ['orderId' => $orderId]),
                "webhookUrl" => $webhookUrl,
                'metadata' => $orderId, // send along the order id
            ]);

            // update the order in the database, add the mollie id
            db()->query('UPDATE orders SET mollie_id = :mollie_id WHERE id = :id', [
                'mollie_id' => $payment->id,
                'id' => $orderId
            ])->update();

            // put all the products in the database
            foreach ($_SESSION['cart']['products'] as $productId => $product) {
                db()->query('INSERT INTO `orders_products`
					(order_id, product_id, price, quantity, created_at, updated_at)
					VALUES
					(:order_id, :product_id, :price, :quantity, :created_at, :updated_at)', [
                    'order_id' => $orderId,
                    'product_id' => $productId,
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ])->insert();
            }

            db()->commit();

            header("Location: " . $payment->getCheckoutUrl(), true, 303);

        } catch (Exception $e) {
            db()->rollBack();
            dd($e->getMessage());
        }
    }

    // return point for mollie. Redirect to the correct page.
    public function done($orderId)
    {
        $order = db()->query('SELECT * FROM `orders` WHERE id = :id', ['id' => $orderId])->first('Order');

        if (in_array($order->payment_status, ['paid'])) {
            Cart::reset();
            return router()->redirect()->name('pay.success', ['orderId' => $orderId]);
        } elseif (in_array($order->payment_status, ['canceled'])) {
            Cart::reset();
            return router()->redirect()->name('pay.canceled', ['orderId' => $orderId]);
        }
        return router()->redirect()->name('pay.failed', ['orderId' => $orderId]);
    }

    // return point for mollie when payment succeeds
    public function success($orderId)
    {
        return response()->view('success');
    }

    // return point for mollie when payment has been canceled
    public function canceled($orderId)
    {
        return response()->view('canceled');
    }

    // return point for mollie when payment failed
    public function failed($orderId)
    {
        return response()->view('failed');
    }

    // return point for mollie
    public function webhook($orderId)
    {

        try {
            $mollieId = router()->parameters()['id'];

            // get the payment
            $mollie = new \Mollie\Api\MollieApiClient();
            $mollie->setApiKey($this->mollieApiKey);
            $payment = $mollie->payments->get($mollieId);

            // update order in database with new status
            db()->query('UPDATE orders SET payment_status = :payment_status WHERE mollie_id = :mollie_id', [
                'payment_status' => $payment->status,
                'mollie_id' => $mollieId,
            ])
                ->update();

        } catch (ApiException $e) {
            dd("API call failed: " . htmlspecialchars($e->getMessage()));
        }
    }
}
