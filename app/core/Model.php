<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 20/05/2018
 * Time: 20:26
 */

class Model
{
    public $data;

    public function first()
    {
        return $this->data[0];
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}