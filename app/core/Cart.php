<?php

class Cart
{

    // actual addtocart function
    public static function addToCart($id, $quantity = 1)
    {
        if (isset($_SESSION['cart']['count'])) {
            $_SESSION['cart']['count'] += $quantity;
        }
        if (isset($_SESSION['cart']['products'][$id])) {
            $_SESSION['cart']['products'][$id]['quantity'] += $quantity;
        } else {
            $product = db()->query('SELECT * FROM products WHERE id = :id', ['id' => $id])
                ->first('Product');

            $_SESSION['cart']['products'][$id] = [
                'quantity' => 1,
                'image' => $product->image,
                'title' => $product->title,
                'price' => $product->price,
                'id' => $product->id
            ];


        }

        self::calculate();
    }

    // actual removefromcart function
    public static function removeFromCart($id, $quantity = 1)
    {
        if ($_SESSION['cart']['count'] > 1) {
            $_SESSION['cart']['count'] -= $quantity;
        }

        if ($_SESSION['cart']['products'][$id]['quantity'] > 1) {
            $_SESSION['cart']['products'][$id]['quantity'] -= $quantity;
        } else {
            unset($_SESSION['cart']['products'][$id]);
        }

        self::calculate();
    }

    // get the cart function
    public static function get()
    {
        return $_SESSION['cart'];
    }

    // calculate the price for each product, then sum up it as a total price
    private static function calculate()
    {
        $totalPrice = 0;
        foreach ($_SESSION['cart']['products'] as $key => $value) {
            $totalPrice += $value['price'] * $value['quantity'];
        }

        $_SESSION['cart']['total'] = $totalPrice;

        if (empty($_SESSION['cart']['products'])) {
            $_SESSION['cart']['count'] = 0;
        }
    }

    // reset values to 0 after payment or empty cart call
    public static function reset()
    {
        // default cart create
        if (isset($_SESSION['cart'])) {
            unset($_SESSION['cart']);
        }
        if (!isset($_SESSION['cart'])) {

            $_SESSION['cart'] = [
                'products' => [],
                'total' => 0.00,
                'count' => 0,
            ];
        }
    }
}
