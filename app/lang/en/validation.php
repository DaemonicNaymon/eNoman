<?php

return [

    'required' => 'This field is required',
    'number' => 'This isn\'t a valid number',
    'email' => 'This isn\'t a valid e-mail address',
    'name' => 'This isn\'t a valid name',
    'min' => 'This value is too short',
    'max' => 'This value is too long',
    'confirm' => 'The two fields don\'t match',
    'postcode' => 'This isn\'t a valid postcode'

];
