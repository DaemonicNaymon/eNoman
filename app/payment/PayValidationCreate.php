<?php
include_once PATH_APP_CORE . "Validation.php";

class PayValidationCreate extends Validation
{

    public function validate()
    {
        // only run code below when the checkout form makes a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['checkout_place_order'])) {
            return [
                'first_name' => ['required', 'name', 'min:2', 'max:25'],
                'suffix_name' => ['name', 'min:2', 'max:15'],
                'last_name' => ['required', 'name', 'min:2', 'max:50'],
                'country' => ['required', 'min:2', 'max:15'],
                'city' => ['required', 'name', 'min:2', 'max:70'],
                'postcode' => ['required', 'postcode', 'min:6', 'max:7'],
                'address' => ['required', 'name', 'min:2', 'max:70'],
                'house_number' => ['required', 'number', 'min:1', 'max:5'],
                'house_number_suffix' => ['min:1', 'max:50'],
                'email' => ['required', 'email', 'min:9', 'max:150'],
                'password' => ['required', 'confirm', 'min:8', 'max:150'],
            ];
        }
    }
}

new PayValidationCreate;
