<?php
/*
 * defining named constants because I want to have the folder paths absolute. every file has its place.
 */
define('PATH_ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('PATH_VENDOR', PATH_ROOT . 'vendor' . DIRECTORY_SEPARATOR);
define('PATH_APP', PATH_ROOT . 'app' . DIRECTORY_SEPARATOR);
define('PATH_APP_CONFIG', PATH_APP . 'config' . DIRECTORY_SEPARATOR);
define('PATH_APP_CONTROLLERS', PATH_APP . 'controllers' . DIRECTORY_SEPARATOR);
define('PATH_APP_MODELS', PATH_APP . 'models' . DIRECTORY_SEPARATOR);
define('PATH_APP_VIEWS', PATH_APP . 'views' . DIRECTORY_SEPARATOR);
define('PATH_APP_CORE', PATH_APP . 'core' . DIRECTORY_SEPARATOR);
define('PATH_APP_PAYMENT', PATH_APP . 'payment' . DIRECTORY_SEPARATOR);
define('PATH_APP_LANG', PATH_APP . 'lang' . DIRECTORY_SEPARATOR);

// loading in classes
require PATH_VENDOR . 'autoload.php';
require PATH_APP_CORE . 'Database.php';
require PATH_APP_CORE . 'Memory.php';
require PATH_APP_CORE . 'Router.php';
require PATH_APP_CORE . 'Respond.php';
require PATH_APP_CORE . 'Model.php';
require PATH_APP_CORE . 'Cart.php';
require PATH_APP_CORE . 'Controller.php';

// debugging tool: var_dump and die after
function dd($text)
{
    if (is_array($text) || is_object($text)) {
        var_dump($text);
        die();
    } else {
        die($text);
    }

}
// creating default session for cart and router
function createDefaultSession()
{
    if (!isset($_SESSION['cart'])) {
        // default cart create
        $_SESSION['cart'] = [
            'products' => [],
            'total' => 0.00,
            'count' => 0,
        ];
    }
    if (!isset($_SESSION['router'])) {
        $_SESSION['router'] = [
            'values' => [],
            'errors' => []
        ];
    }
}

function asset($filepath)
{
    return router()->domain . router()->publicPath . trim($filepath, '/');
}

function router()
{
    return Memory::getClass('Router');
}

function response($content = '', $httpResponseCode = 200)
{
    return Memory::getClass('Respond')->response($content, $httpResponseCode);
}

function db()
{
    return Memory::getClass('Database');
}

function user()
{
    return Memory::getClass('User');
}

function lang($filename)
{
    return Memory::getFile(PATH_APP_LANG . 'en/' . $filename);
}

session_start();
createDefaultSession();
router()->run();
