<?php

/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 08/06/2018
 * Time: 19:17
 */
class User extends Model
{
    protected $email;
    protected $password;
    protected $user;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getUser()
    {
        return $this->user;
    }

    protected function checkCredentials()
    {
        db()->query('SELECT * FROM `users` WHERE email = ?');
    }
}